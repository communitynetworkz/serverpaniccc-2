<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//namespace Autoloader;

/**
 * An namespace based  Autoloader.
 * 
 * @see https://thomashunter.name/blog/simple-php-namespace-friendly-autoloader-class/
 *
 * @author or better modifier: Wouter van den brink, wouter@van-den-brink.net
 * 
 */
class Autoloader {
    //put your code here
    static public function loader($className) {
        
      if(mb_substr($className,0,mb_strlen('Serverpokedalarm\\Classes\\'))=='Serverpokedalarm\\Classes\\'){
          //echo 'Class';
         // echo "\n";
         $filename = "".str_replace('\\','/', $className).".php";
          if (file_exists($filename)) {
            include($filename);
            //echo $className;
            if (class_exists($className)) {
                return true;
            }
         }
      }elseif(mb_substr($className,0,mb_strlen('Serverpokedalarm\\Interfaces\\'))=='Serverpokedalarm\\Interfaces\\'){
          //echo 'Interface';
          $filename = "".str_replace('\\','/', $className).".php";
          
          if (file_exists($filename)) {
            
            include($filename);
            //echo $className
            if (interface_exists($className)) {
                
                return true;
            }
         }
      }elseif(mb_substr($className,0,mb_strlen('Serverpokedalarm\\Traits\\'))=='Serverpokedalarm\\Traits\\'){
          //echo 'Trait';
           $filename = "".str_replace('\\','/', $className).".php";
          if (file_exists($filename)) {
            
            include($filename);
            //echo $className;
            if (trait_exists($className)) {
                
                return true;
            }
         }
      }else{echo 'false';}
        $filename = "Classes".str_replace('\\','/', $className).".php";
        
        return $filename;
    }
}
