*This is a first try to write a functional specification.* 

*Inspired by [http://www.joelonsoftware.com/articles/WhatTimeIsIt.html](http://www.joelonsoftware.com/articles/WhatTimeIsIt.html).*

---

#ServerPanic#
## Functional Specification ##
Wouter van den Brink

Last Updated: 5 dec, 2014

####\- C O N F I D E N T I A L \-####
2014, [globalworldwideweb.net](http://globalworldwideweb.net/serverpanic),  All Rights Reserved. 

---


###Overview###

***ServerPanic*** is a server monitoring system. It consists of an Android app , a  monitoring web-service  and some remote signaling web-apps on each of your servers. When running, the Android app will consult the monitoring web-service. The monitoring webservice will consult  the remote signaling web apps on your servers and report back to the Android app. The Android app in turn will warn you when there is a problem with one or more of your servers. 


#####Elaborated:#####

The ***ServerPanic Android app*** consults a ***ServerPanic web-service***. 

The  ***ServerPanic web-service*** consults a number of web-apps (***ServerPanic local agents***) running on each of your servers. 

Each ***ServerPanic local agent*** checks the operating system. Then  it decides whether to generate an alarm (***ServerPanic indication***) or not. After evaluation and decision making, the  local agent outputs a report (with of without the alarm)  back to the  ***ServerPanic web-service***.
 
The ***ServerPanic web-service*** receives the reports of the consulted  ***ServerPanic local agents***. Then, if there are any  ***ServerPanic indications***, it adds a ***ServerPanic indication*** to it's own accumulated report back to the Android app.

The ***ServerPanic Android app*** receives the accumulated report and the possible ***ServerPanic indication***. It wil display the report and if there is a ***ServerPanic indication*** the Android app will create a system alert, warning the phone user. 







---

###Scenarios###
To illustrate the use cases some scenarios.
####Scenario 1: Mathilda.####
ToDo: write a scenario..., get to know Mathilda (is she german?). 


---
###Components###
####ServerPanic Local Agent####
The local agent component of the system has three duties:
  
>1. perform checks on the server  
2. decide on raising an alarm or not 
3. report back to caller (***ServerPanic web-service***)
  
The agent consist of a php script that performs checks on the health of the server. For this the script uses php functions and system calls or command line commands. 
>***Technical note***  
>php has some useful functions itself.  
On the other hand it is possible to perform command line commands and retrieve the output thereof into php.  

The component should check things like  
  
>* Disc space of important discs
* vitality of indicated deamons: e.g. apache, mysqld
* cpu usage
* RAM availability and usage

---

####ServerPanic web-service####

The ServerPanic web-service is the center of the system.  
>1. It receives consultations from the ***ServerPanic Android app*** (possibly multiple instances on multiple phones). The consultations asks information for certain servers.  
2. The web-service consults the local agents on the appropriate servers.
3. The web-service evaluates responses from the local agents to decide to raise an alarm or not.
4. The web-service reports back to the Android app, with of without an alarm. 

The web-service consists of a php script that receives consultations, performs calls to local agents and reports back to Android apps, via http(s). 

>***Technical note***  
Http(s) communication of the php script with the local agents can be via use of the curl php-extention, the http php-extension (version 1.7 or 2+ differ in very high degree), and others. 

---


####ServerPanic Android app####

The ***ServerPanic Android app*** runs on the Android phone and  
>1. tests internet access  
2. is continuously consulting (and thus testing) the ***ServerPanic web-service***, with an time interval, 
3. is receiving back reports from the web-service and
4. raises an alarm if appropriate. 

>***Technical note***  
For now the Android app is developed using Google App Inventor 2. Later we might use the phonegap/cordova system. (Phonegap/cordova allows development with html and javascript and has the possibility to create app for different platforms besides Android)

>***Technical note***  
The ***ServerPanic Android app*** is distributed as an apk file on our own server, not via google play store. The apk file's http address can be presented as a qr code and installed on the phone using it's camera. Experimental version (minus alpha):  [http://globalworldwideweb.net/serverpanic/apks/qrqddress.php](http://globalworldwideweb.net/serverpanic/apks/qrqddress.php "qr-code to apk file")